﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace BenchmarkList
{
    public struct Foo
    {
        public int a;
        public string b;
        public long c;
        public short d;
    }


    public class PushBenchmark
    {
        private static int result; 

        [Benchmark]
        public void Stack100Int()
        {
            TestStack<int>( 100 );
        }

        [Benchmark]
        public void Stack100_000Int()
        {
            TestStack<int>( 100000 );
        }

        [Benchmark]
        public void Stack1_000_000Int()
        {
            TestStack<int>( 1000000 );
        }


        [Benchmark]
        public void Stack100Struct()
        {
            TestStack<Foo>( 100 );
        }

        [Benchmark]
        public void Stack100_000Struct()
        {
            TestStack<Foo>( 100000 );
        }

        [Benchmark]
        public void Stack1_000_000Struct()
        {
            TestStack<Foo>( 1000000 );
        }


        [Benchmark]
        public void List100Int()
        {
            TestLinkedList<int>( 100 );
        }

        [Benchmark]
        public void List100_000Int()
        {
            TestLinkedList<int>( 100000 );
        }

        [Benchmark]
        public void List1_000_000Int()
        {
            TestLinkedList<int>( 1000000 );
        }


        [Benchmark]
        public void List100Struct()
        {
            TestLinkedList<Foo>( 100 );
        }

        [Benchmark]
        public void List100_000Struct()
        {
            TestLinkedList<Foo>( 100000 );
        }

        [Benchmark]
        public void List1_000_000Struct()
        {
            TestLinkedList<Foo>( 1000000 );
        }



        private void TestStack<T>( int len )
        {
            Stack<T> l = new Stack<T>();

            for( int i = 0; i < len; i++ )
            {
                l.Push( default( T ) );
            }

            PushBenchmark.result = l.Count;
        }

        private void TestLinkedList<T>( int len )
        {
            LinkedList<T> l = new LinkedList<T>();

            for( int i = 0; i < len; i++ )
            {
                l.AddFirst( default( T ) );
            }
            PushBenchmark.result = l.Count;
        }
    }

    public class PushPopBenchmark
    {
        public static int result;

        [Benchmark]
        public void Stack100Int()
        {
            TestStack<int>( 100 );
        }

        [Benchmark]
        public void Stack100_000Int()
        {
            TestStack<int>( 100000 );
        }

        [Benchmark]
        public void Stack1_000_000Int()
        {
            TestStack<int>( 1000000 );
        }


        [Benchmark]
        public void Stack100Struct()
        {
            TestStack<Foo>( 100 );
        }

        [Benchmark]
        public void Stack100_000Struct()
        {
            TestStack<Foo>( 100000 );
        }

        [Benchmark]
        public void Stack1_000_000Struct()
        {
            TestStack<Foo>( 1000000 );
        }


        [Benchmark]
        public void List100Int()
        {
            TestLinkedList<int>( 100 );
        }

        [Benchmark]
        public void List100_000Int()
        {
            TestLinkedList<int>( 100000 );
        }

        [Benchmark]
        public void List1_000_000Int()
        {
            TestLinkedList<int>( 1000000 );
        }


        [Benchmark]
        public void List100Struct()
        {
            TestLinkedList<Foo>( 100 );
        }

        [Benchmark]
        public void List100_000Struct()
        {
            TestLinkedList<Foo>( 100000 );
        }

        [Benchmark]
        public void List1_000_000Struct()
        {
            TestLinkedList<Foo>( 1000000 );
        }



        private void TestStack<T>( int len )
        {
            Stack<T> l = new Stack<T>();

            for( int i = 0; i < len; i++ )
            {
                l.Push( default( T ) );
            }
            
            for( int i = 0; i < len; i++ )
            {
                l.Pop();
            }
            PushPopBenchmark.result = l.Count;
        }

        private void TestLinkedList<T>( int len )
        {
            LinkedList<T> l = new LinkedList<T>();

            for( int i = 0; i < len; i++ )
            {
                l.AddFirst( default( T ) );
            }

            for( int i = 0; i < len; i++ )
            {
                l.RemoveFirst();
            }
            PushPopBenchmark.result = l.Count;
        }
    }


    class Program
    {
        static void Main( string[] args )
        {
            var summary = BenchmarkRunner.Run<PushBenchmark>();
            var summary2 = BenchmarkRunner.Run<PushPopBenchmark>();
        }
    }
}
