#![feature(test)]
extern crate test;
use std::collections::LinkedList;
use std::ops::AddAssign;

#[derive(Default)]
struct Foo {
    _a: i32,
    _b: String,
    _c: i64,
    _d: i16
}

impl AddAssign for Foo {
    fn add_assign(&mut self, other: Foo) {
        *self = Foo{
            _a: self._a + other._a,
            _b: self._b.clone(),
            _c: self._c + other._c,
            _d: self._d + other._d,
        }
    }
}

#[cfg(test)]
mod push_pop_benchmark {
    use super::*;

    fn test_stack<T: Default + AddAssign>(len: u32 ) {
        let mut l = vec![];

        for _ in 0..len {
            l.push(T::default());
        }

        let mut sum = T::default();
        for _ in 0..len {
            sum += l.pop().unwrap(); // We know the length, so let's be lazy.
        }

        test::black_box(sum);
    }

    fn test_list<T: Default + AddAssign>(len: u32 ) {
        let mut l: LinkedList<T> = LinkedList::new();

        for _ in 0..len {
            l.push_front(T::default());
        }

        let mut sum = T::default();
        for _ in 0..len {
            sum += l.pop_front().unwrap();
        }

        test::black_box(sum);
    }

    #[bench]
    fn stack_100_int(b: &mut test::Bencher) {
        b.iter(|| test_stack::<i32>(100));
    }

    #[bench]
    fn stack_100_000_int(b: &mut test::Bencher) {
        b.iter(|| test_stack::<i32>(100_000));
    }

    #[bench]
    fn stack_1_000_000_int(b: &mut test::Bencher) {
        b.iter(|| test_stack::<i32>(1_000_000));
    }

    #[bench]
    fn stack_100_struct(b: &mut test::Bencher) {
        b.iter(|| test_stack::<Foo>(100));
    }

    #[bench]
    fn stack_100_000_struct(b: &mut test::Bencher) {
        b.iter(|| test_stack::<Foo>(100_000));
    }

    #[bench]
    fn stack_1_000_000_struct(b: &mut test::Bencher) {
        b.iter(|| test_stack::<Foo>(1_000_000));
    }



    #[bench]
    fn list_100_int(b: &mut test::Bencher) {
        b.iter(|| test_list::<i32>(100));
    }

    #[bench]
    fn list_100_000_int(b: &mut test::Bencher) {
        b.iter(|| test_list::<i32>(100_000));
    }

    #[bench]
    fn list_1_000_000_int(b: &mut test::Bencher) {
        b.iter(|| test_list::<i32>(1_000_000));
    }

    #[bench]
    fn list_100_struct(b: &mut test::Bencher) {
        b.iter(|| test_list::<Foo>(100));
    }

    #[bench]
    fn list_100_000_struct(b: &mut test::Bencher) {
        b.iter(|| test_list::<Foo>(100_000));
    }

    #[bench]
    fn list_1_000_000_struct(b: &mut test::Bencher) {
        b.iter(|| test_list::<Foo>(1_000_000));
    }
}

#[cfg(test)]
mod push_benchmark {
    use super::*;

    fn test_stack<T: Default>(len: u32 ) {
        let mut l = vec![];

        for _ in 0..len {
            l.push(T::default());
        }

        test::black_box(l.len());
    }

    fn test_list<T: Default>(len: u32 ) {
        let mut l: LinkedList<T> = LinkedList::new();

        for _ in 0..len {
            l.push_front(T::default());
        }

        test::black_box(l.len());
    }

    #[bench]
    fn stack_100_int(b: &mut test::Bencher) {
        b.iter(|| test_stack::<i32>(100));
    }

    #[bench]
    fn stack_100_000_int(b: &mut test::Bencher) {
        b.iter(|| test_stack::<i32>(100_000));
    }

    #[bench]
    fn stack_1_000_000_int(b: &mut test::Bencher) {
        b.iter(|| test_stack::<i32>(1_000_000));
    }

    #[bench]
    fn stack_100_struct(b: &mut test::Bencher) {
        b.iter(|| test_stack::<Foo>(100));
    }

    #[bench]
    fn stack_100_000_struct(b: &mut test::Bencher) {
        b.iter(|| test_stack::<Foo>(100_000));
    }

    #[bench]
    fn stack_1_000_000_struct(b: &mut test::Bencher) {
        b.iter(|| test_stack::<Foo>(1_000_000));
    }



    #[bench]
    fn list_100_int(b: &mut test::Bencher) {
        b.iter(|| test_list::<i32>(100));
    }

    #[bench]
    fn list_100_000_int(b: &mut test::Bencher) {
        b.iter(|| test_list::<i32>(100_000));
    }

    #[bench]
    fn list_1_000_000_int(b: &mut test::Bencher) {
        b.iter(|| test_list::<i32>(1_000_000));
    }

    #[bench]
    fn list_100_struct(b: &mut test::Bencher) {
        b.iter(|| test_list::<Foo>(100));
    }

    #[bench]
    fn list_100_000_struct(b: &mut test::Bencher) {
        b.iter(|| test_list::<Foo>(100_000));
    }

    #[bench]
    fn list_1_000_000_struct(b: &mut test::Bencher) {
        b.iter(|| test_list::<Foo>(1_000_000));
    }
}