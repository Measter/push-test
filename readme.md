# Array-based vs Linked-List-Based Benchmark

These tests were written to benchmark the performance of using an array as a stack vs using a linked-list as a stack. The contention being whether the cache-friendly nature of arrays beat the algorithmic simplicity of inserting to a linked-list.

This test was done using both C\# and Rust, to test both under a garbage collected environment and a more manually managed environment.

All tests use the standard structures provided by each language's standard library, with 100, 100,000, 1,000,000, and 10,000,000 elements. All tests are run with storing a 32-bit integer, and a struct with a 16-bit, 32-bit, 64-bit integers, and a String.

The struct is initialised to the default value, which in C\# results in 0 for the numbers, and null for the String. In Rust, due to String not being nullable, default stores an empty string.

For C\#, I used the [BenchmarkDotNet](http://benchmarkdotnet.org/) library, under Rust I used the benchmarking provided by Cargo.

All tests were done with release-mode compilation, in 64-bit, on Windows 10 version 1703 (build 15063.296), running on an Intel i7 6700k, 16GB RAM. The C\# version was compiled against .Net 4.6.2, running under .Net Framework 4.7. The Rust versions were compiled by the nightly 2017-05-20, against the MSVC runtime.

**NOTE** I have removed the 10,000,000 result, as they took a long time without adding new information.

## Push Test

In this benchmark we test pushing N elements to the stack. The results are as follows:

## CSharp

**Array-Based**

                Method |               Mean |             Error |            StdDev |
----------------------:|-------------------:|------------------:|------------------:|
          Stack100Int |         408.9 ns |         2.169 ns |         1.811 ns |
      Stack100_000Int |     398,049.9 ns |     4,421.829 ns |     4,136.181 ns |
    Stack1_000_000Int |   4,728,605.9 ns |    38,563.136 ns |    36,071.978 ns |
       Stack100Struct |       1,065.3 ns |        10.606 ns |         9.921 ns |
   Stack100_000Struct |   2,361,643.5 ns |    20,191.061 ns |    18,886.729 ns |
 Stack1_000_000Struct |  24,334,547.5 ns |   478,299.253 ns |   862,472.214 ns |

**Linked-List-Based**

                Method |               Mean |             Error |            StdDev |
----------------------:|-------------------:|------------------:|------------------:|
           List100Int |       1,493.4 ns |         7.933 ns |         7.420 ns |
       List100_000Int |   3,413,698.0 ns |    38,445.864 ns |    35,962.282 ns |
     List1_000_000Int | 104,778,723.5 ns | 2,075,036.126 ns | 2,975,954.808 ns |
        List100Struct |       2,450.4 ns |        16.201 ns |        14.362 ns |
    List100_000Struct |   5,384,004.1 ns |    31,787.183 ns |    29,733.748 ns |
  List1_000_000Struct | 139,391,469.3 ns | 2,766,612.449 ns | 2,310,246.473 ns |


## Rust

**Array-Based**

    test push_benchmark::stack_100_int              ... bench:         467 ns/iter (+/- 10)
    test push_benchmark::stack_100_000_int          ... bench:     158,428 ns/iter (+/- 2,570)
    test push_benchmark::stack_1_000_000_int        ... bench:   2,755,460 ns/iter (+/- 164,805)
    test push_benchmark::stack_100_struct           ... bench:         741 ns/iter (+/- 16)
    test push_benchmark::stack_100_000_struct       ... bench:   2,188,057 ns/iter (+/- 384,837)
    test push_benchmark::stack_1_000_000_struct     ... bench:  24,337,051 ns/iter (+/- 3,602,919)

**Linked-List-Based**

    test push_benchmark::list_100_int               ... bench:       4,693 ns/iter (+/- 5,169)
    test push_benchmark::list_100_000_int           ... bench:   4,844,298 ns/iter (+/- 1,799,798)
    test push_benchmark::list_1_000_000_int         ... bench:  52,585,585 ns/iter (+/- 6,584,418)
    test push_benchmark::list_100_struct            ... bench:       5,293 ns/iter (+/- 526)
    test push_benchmark::list_100_000_struct        ... bench:   5,681,855 ns/iter (+/- 962,291)
    test push_benchmark::list_1_000_000_struct      ... bench:  64,996,186 ns/iter (+/- 9,340,698)


## Push-Pop Test

In this benchmark, we test pushing N elements to the stack, then popping them off.

### CSharp

**Array-Based**

                Method |               Mean |             Error |            StdDev |
---------------------- |-------------------:|------------------:|------------------:|
          Stack100Int |         672.3 ns |        13.75 ns |        12.859 ns |
      Stack100_000Int |     607,163.0 ns |     6,649.38 ns |     6,219.832 ns |
    Stack1_000_000Int |   6,962,322.1 ns |    62,772.31 ns |    58,717.249 ns |
       Stack100Struct |       3,652.6 ns |        46.66 ns |        43.646 ns |
   Stack100_000Struct |   4,166,850.7 ns |    24,257.08 ns |    21,503.270 ns |
 Stack1_000_000Struct |  41,054,505.2 ns |   154,485.39 ns |   120,612.021 ns |

**Linked-List-Based**

                Method |               Mean |             Error |            StdDev |
---------------------- |-------------------:|------------------:|------------------:|
           List100Int |       2,235.3 ns |        28.94 ns |        25.656 ns |
       List100_000Int |   3,889,133.9 ns |    53,411.27 ns |    49,960.931 ns |
     List1_000_000Int |  98,807,916.7 ns | 1,940,743.69 ns | 1,906,069.623 ns |
        List100Struct |       3,068.6 ns |        11.02 ns |         9.768 ns |
    List100_000Struct |   5,603,687.7 ns |   109,540.51 ns |   183,017.578 ns |
  List1_000_000Struct | 140,831,042.6 ns | 2,717,339.13 ns | 3,719,524.478 ns |

## Rust

**Array-Based**

    test push_pop_benchmark::stack_100_int          ... bench:         545 ns/iter (+/- 353)
    test push_pop_benchmark::stack_100_000_int      ... bench:     216,718 ns/iter (+/- 6,311)
    test push_pop_benchmark::stack_1_000_000_int    ... bench:   3,448,601 ns/iter (+/- 215,608)
    test push_pop_benchmark::stack_100_struct       ... bench:       1,586 ns/iter (+/- 79)
    test push_pop_benchmark::stack_100_000_struct   ... bench:   2,967,087 ns/iter (+/- 518,763)
    test push_pop_benchmark::stack_1_000_000_struct ... bench:  32,293,331 ns/iter (+/- 1,915,517)

**Linked-List-Based**

    test push_pop_benchmark::list_100_int           ... bench:       4,692 ns/iter (+/- 655)
    test push_pop_benchmark::list_100_000_int       ... bench:   4,695,565 ns/iter (+/- 176,715)
    test push_pop_benchmark::list_1_000_000_int     ... bench:  51,806,906 ns/iter (+/- 12,014,102)
    test push_pop_benchmark::list_100_struct        ... bench:       6,148 ns/iter (+/- 557)
    test push_pop_benchmark::list_100_000_struct    ... bench:   6,302,846 ns/iter (+/- 843,833)
    test push_pop_benchmark::list_1_000_000_struct  ... bench:  71,215,960 ns/iter (+/- 15,229,205)

## Conclusion

This data supports the claim that the cache-friendly nature of arrays far outweighs the time-complexity of resizing the array.